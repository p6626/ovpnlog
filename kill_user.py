from telnetlib import Telnet
import subprocess
import re
import os

ovpn_host = os.environ['TELNET_OVPN_HOST']
ovpn_port = os.environ['TELNET_OVPN_PORT']
d_user=os.environ['D_USER']
status_log = os.environ['STATUS_LOG']
username=""

def parameters(username):
    """Telnet command deleting users
    """
    command='kill {}'.format(username)
    return command

def to_bytes(line):
    """Update strin to utf8
    """
    return f"{line}\n".encode("utf-8")

def dell_user(client):
    """FUNCTION dell user.
    Runs script bash at d_user
    """
    return subprocess.run([d_user, "".join((re.findall(r'[0-9]+(?:\.[0-9]+){3}', client)))]).returncode

try:
    """Telnet connect to the openvpn manager
    """
    with Telnet(ovpn_host, ovpn_port) as manager:
        manager.write(to_bytes(parameters(username)))
        all_result = manager.read_very_eager().decode('utf-8')
        print (all_result)
except:
    pass

def clear_file (username):
    """Overwrite file log with username(dell str with username)
    """
    with open(status_log, 'r') as log_fiel:
        lines = log_fiel.readlines()

    dell_str=re.compile(re.escape(username))

    with open(status_log, 'w') as f:
        for line in lines:
            result = dell_str.search(line)
            if result is None:
                f.write(line)

def main(username):
    if username is None:
        print ("User is missing:", username)
        pass
    else:
        parameters(username)
        dell_user(username)
        clear_file(username)

if __name__ == '__main__':
    main(username)