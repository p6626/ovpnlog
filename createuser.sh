#!/usr/bin/env bash

if [ "x$1" == "x" ];
then
    echo "No user defined";
    exit 1;
else
    client=$1
    vpn_server="$OVPN_HOST"
    vpn_port="$OVPN_PORT"
    vpn_proto="$OVPN_PROTO"
    vpn_mtu="$OVPN_MTU"
    vpn_cipher="AES-256-GCM:AES-128-GCM"
    vpn_auth="SHA256"
    mkdir -p "$USERS_DIR"$client
    mkdir -p "$USERS_DIR"archive
    cd "$EASY_RSA_DIR" && . ./vars
    export KEY_DIR=/app/openvpn/easy-rsa/keys/
    cd "$EASY_RSA_DIR" && ./build-key --batch $client

echo "#Client config
client     #Программа выступает в роли клиента.
resolv-retry infinite     #Преобразовать имя хоста сервера OpenVPN.
remote $vpn_server     #IP OpenVPN СЕРВЕРА.

#Connect config
port $vpn_port     #Server connect port.
proto $vpn_proto     #Works proto.
dev tun     #Create a routed IP tunnel.
nobind     #Do not bind to local address and port.
remote-cert-tls server     #For a client to verify that only hosts with a server certificate can connect.
persist-key
persist-tun
comp-lzo no     #Enable LZO compression algorithm.
tun-mtu $vpn_mtu     #Take the TUN device MTU.

#Cert config (if keys outside)
tls-client
# ca /etc/openvpn/client/ca.crt     #Путь до сертификатов.
# tls-auth /etc/openvpn/client/ta.key 1     #Ключ для дополнительной защиты.
# cert /etc/openvpn/client/$client.crt     #Публичный ключ клиента.
# key /etc/openvpn/client/$client.key     #Этот файл нужно хранить в секрете.

#Client config
keepalive 60 120     #Пинговать удалённый узел каждые 60 секунд и считать его упавшим, если он не ответил за 120 секунд.

#Encryption config
#data-ciphers $vpn_cipher     #Выбор криптографических шифров. для версии >2.5.
auth $vpn_auth     #The OpenVPN data channel protocol uses encrypt-then-mac (i.e. first encrypt a packet then HMAC the resulting ciphertext), which prevents padding oracle attacks.

#Log config
status /var/log/openvpn/openvpn-status.log     #Файл текущего статуса. Обрезается и перезаписывается каждую минуту.
log-append  /var/log/openvpn/openvpn-append.log     #Означает дополнение журнала.
verb 3
" > "$USERS_DIR"$client/openvpn.conf


printf "<cert>\n$(grep -Pzo "(-----BEGIN CERTIFICATE-----(.|\n)*\n-----END CERTIFICATE-----)" $KEY_DIR$client.crt)\n</cert>\n" >> "$USERS_DIR"$client/openvpn.conf
printf "<key>\n$(cat $KEY_DIR$client.key)\n</key>\n" >> "$USERS_DIR"$client/openvpn.conf
printf "<ca>\n$(cat $KEY_DIR/ca.crt)\n</ca>\n" >> "$USERS_DIR"$client/openvpn.conf
printf "key-direction 1\n" >> "$USERS_DIR"$client/openvpn.conf
printf "<tls-auth>\n$(cat $KEY_DIR/ta.key)\n</tls-auth>\n" >> "$USERS_DIR"$client/openvpn.conf

mv "$KEY_DIR"$client.crt "$USERS_DIR"$client/
mv "$KEY_DIR"$client.key "$USERS_DIR"$client/
mv "$KEY_DIR"$client.csr "$USERS_DIR"$client/
cp "$KEY_DIR"ca.crt "$USERS_DIR"$client/
cp "$KEY_DIR"ta.key "$USERS_DIR"$client/

s_dir="$USERS_DIR"$client/
d_dir="$USERS_DIR"$client.tar

for i in $s_dir*; do
  tar -rv -f $d_dir -C $s_dir ${i##*/}
done

mv "$USERS_DIR"$client.tar "$USERS_DIR"archive

fi