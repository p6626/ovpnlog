from flask import Flask, request, render_template, redirect, jsonify, send_from_directory, url_for
import openvpn_stats, create_user, kill_user
import json
import logging
import os

app = Flask(__name__)


@app.route('/', methods=['GET'])
def log():
    """Main page
    Return: user online and button dell
    """
    openvpn_stats.readfile()
    data = openvpn_stats.common_name()
    users=openvpn_stats.user_list()
    return render_template('main.html', data=data, users=users)

@app.route('/user/<username>', methods=['GET', 'DELETE', 'PUT', 'POST'])
def users(username):
    """Delete user function
    Return: delete user button
    """
    if request.method == 'PUT':
        pass
    elif request.method == 'DELETE':
        try:
            kill_user.main(username)
            return json.dumps({'success':True})
        except:
            print ("Vizualization, kill user error")
    elif request.method == 'POST':
        app.config['ARCHIVE_DIR']='openvpn/easy-rsa/keys/archive/'
        download = os.path.join(app.root_path, app.config['ARCHIVE_DIR'])
        return send_from_directory(download, username)
    return json.dumps({'success':False})

@app.route('/new_user/', methods=['GET', 'POST'])
def new_user():
    """Add user function
    Return: New user and create keys
    """
    if request.method == 'POST':
        try:
            create_user.a_user(request.form['username'])
            return redirect('/', code=302)     
        except:
            return ''
    else:
        return redirect('/', code=302)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port="5000", debug=True)