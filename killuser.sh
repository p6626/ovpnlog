#!/usr/bin/env bash

if [ "x$1" == "x" ];
then 
    echo "No user defined";
    exit 1;
else
    client=$1
    mkdir -p "$USERS_DIR"revoke/$client
    cd /app/openvpn/easy-rsa && . ./vars
    export "$KEY_DIR"
    cd /app/openvpn/easy-rsa && ./revoke-full $client

mv "$USERS_DIR"$client/* "$USERS_DIR"revoke/$client/
rmdir "$USERS_DIR"$client
rm -rf "$USERS_DIR"archive/$client.tar

fi