import re
import os
import subprocess

users_dir = os.environ['USERS_DIR']
c_user = os.environ['C_USER']
username=""

def gen_user(u_name):
    """FUNCTION create user.
    Runs script bash at c_user
    """
    return subprocess.run([c_user, u_name]).returncode

def a_user(client):
    """
    Сhecks if the user exists 
    Runs FUNCTION gen_user
    if user created return 'User added'
    if FUNCTION 'gen_user' return error print ERROR
    """
    if client in os.listdir(users_dir):
        print ("User created")
        return
    if gen_user(client)==0:
        print("User added.")
    else:
        print("Create user ERROR: Сontact your system administrator")

def main():
    a_user(username)

if __name__ == '__main__':
    main()