from prettytable import PrettyTable
import re
import time
import os

status_log = os.environ['STATUS_LOG']
user_path = os.environ['USERS_DIR']
archive_path = os.environ['ARCHIVE_DIR']
log = []
chk = [0, 1]


def readfile(filename=status_log) -> list:
    """sumary_line
    Return: list varible
    """
    log.clear()
    with open(filename, 'r', encoding='utf8') as logfile:
        for i in logfile:
            log.append(i.strip("\n").split(","))
    chk[1] = hash(filename)


def common_name(log=log) -> list:
    """sumary_line
    Return: list varible
    """
    data = {}
    for i in range(len(log)):
        if (re.search('[A-Z]', log[i][0])) or (re.search('(?:[0-9a-fA-F]:?){12}', log[i][0])) or (re.match('10', log[i][0])):
            pass
        else:
            name = (log[i][0])
            data[name] = {}
            data[name]['address'] = ((log[i][1]))
            data[name]['received'] = ((log[i][2]))
            data[name]['sent'] = ((log[i][3]))
            data[name]['connected'] = ((log[i][-1]))
    return data

def user_list(archive_path=archive_path) -> list:
    """All users in OpenVPN
    Return: list users
    """
    archive=[d for d in os.listdir(archive_path) if os.path.isfile(os.path.join(archive_path, d))]
    return archive




def main():

    chk[0] = 1

    while True:

        readfile(status_log)
        if chk[0] == chk[1]:
            continue

        name = common_name(log)
        all_user = user_list(archive_path)

if __name__ == '__main__':
    main()