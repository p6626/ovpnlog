FROM python:3.8-slim-buster

ENV STATUS_LOG=/app/openvpn-status.log
ENV OVPN_HOST=127.0.0.1
ENV OVPN_PORT=7505
ENV D_USER=/app/killuser.sh
ENV USERS_DIR=/app/openvpn/easy-rsa/keys/
ENV C_USER=/app/createuser.sh
ENV OVPN_PROTO=tcp
ENV OVPN_MTU=1500
ENV KEY_DIR=/app/openvpn/keys/
ENV EASY_RSA_DIR=/app/openvpn/easy-rsa/

EXPOSE 5000

WORKDIR /app
COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt
COPY . .
CMD [ "python3", "./vizualization.py" ]